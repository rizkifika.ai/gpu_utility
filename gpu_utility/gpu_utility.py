import psutil
import subprocess
import xmltodict
import json
from dateutil.parser import parse

class GpuUtility():
    def __init__(self):
        gpu_data = dict()
        timestamp = None
        self.gpu_process_list = []
        self.gpus_detail = dict()
        self.gpu_number = 0
    
    def __probe_devices(self):
        cmd = "nvidia-smi -q -x"
        result = psutil.Popen(cmd, stdout=subprocess.PIPE, shell=True)
        xmlstring = result.stdout.read().decode('ascii')
        return xmltodict.parse(xmlstring)

    def make_context(self):
        self.gpu_data = self.__probe_devices()
        self.timestamp = parse(self.gpu_data["nvidia_smi_log"]["timestamp"])
        self.gpu_number = int(self.gpu_data["nvidia_smi_log"]["attached_gpus"])
        self.__get_gpus_detail()
        print(self.timestamp)

    def __get_gpu_utilization(self,gpu_id = 0, gpu_data = []):
        gpu_info = dict()
        gpu_info["type"] = gpu_data['product_name']
        gpu_info["gpu_id"] = gpu_id
        gpu_info["total_memory"] = gpu_data['fb_memory_usage']['total']
        gpu_info["used_memory"] = gpu_data['fb_memory_usage']["used"]
        gpu_info["free_memory"] = gpu_data['fb_memory_usage']["free"]
        try:
            gpu_info["process"] = gpu_data["processes"]["process_info"]
        except:
            gpu_info["process"] = []

        return gpu_info

    def __get_gpus_detail(self):
        gpus_detail = dict()
        gpus_detail["gpu_number"] = self.gpu_number
        gpus_detail["driver_version"] = self.gpu_data["nvidia_smi_log"]["driver_version"]
        gpus_detail["timestamp"] = str(self.timestamp)
        gpus_detail["cuda_version"] = self.gpu_data["nvidia_smi_log"]["cuda_version"]

        listed_process = []
        
        if self.gpu_number == 1:
            result = self.__get_gpu_utilization(gpu_id = 0, gpu_data = self.gpu_data["nvidia_smi_log"]["gpu"])
            listed_process.append(result)

        else:
            num = 0
            while num < self.gpu_number:
                result = self.__get_gpu_utilization(gpu_id = num,gpu_data = self.gpu_data["nvidia_smi_log"]["gpu"][num])
                listed_process.append(result)
                num = num + 1

        gpus_detail["gpus_utilization_detail"] = listed_process

        self.gpus_detail = gpus_detail

        return gpus_detail
            


    def get_devices_info(self):
        return self.gpus_detail

    def probe_pid(self, pid = 0):

        for gpu_utilization in self.gpus_detail["gpus_utilization_detail"]:
            for process in gpu_utilization["process"]:
                # print(process)
                if int(process["pid"]) == int(pid):
                    process["gpu_id"] = gpu_utilization["gpu_id"]
                    process["timestamp"] = str(self.timestamp)
                    process["used_memory"] = int(process["used_memory"][:len(process["used_memory"])-3])
                    return process
        
        return False

if __name__ == "__main__":
    gpu_data = GpuUtility()
    gpu_data.make_context()
    print(gpu_data.probe_pid(pid = 6270)["used_memory"])
    with open('devices.json', 'w') as json_file:
        json.dump(gpu_data.get_devices_info(), json_file)