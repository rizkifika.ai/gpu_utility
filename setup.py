from distutils.core import setup, Extension
from gpu_utility.__init__ import __version__

with open('requirements.txt', 'r') as f:
    reqs = [line.strip() for line in f.readlines()]

setup(

    name="nodeflux.tracker",
    version=__version__,
    packages=['gpu_utility'],
    description='GPU utility library based on nvidia-smi',
    author=["rizkifika asanul in'am"],
    install_requires=reqs
)
